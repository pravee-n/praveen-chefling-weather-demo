package net.chefling.praveenweather.activities;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import net.chefling.praveenweather.R;
import net.chefling.praveenweather.fragments.WeatherCardFragment;
import net.chefling.praveenweather.utils.BasicDataCallback;
import net.chefling.praveenweather.views.GlobalTextView;
import net.chefling.praveenweather.utils.Urls;
import net.chefling.praveenweather.utils.Utilities;
import net.chefling.praveenweather.views.GlobalIconView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bootApp();

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_main);
        fillView();
    }

    private void bootApp() {
        Utilities.setContext(this);
        Utilities.setFontTypeFace();
        Utilities.setIconFontTypeFace();
        Utilities.setIconMap();
    }

    private void fillView() {
        ImageView imageView = (ImageView) findViewById(R.id.am_img);
        Glide.with(this).load(R.drawable.chicago_night_1).into(imageView);
        getData();
    }

    private void getData() {

        BasicDataCallback weatherDataCallback = new BasicDataCallback() {
            @Override
            public void callback(JSONObject data) {
                Log.d("test", data.toString());
                fillData(data);
            }
        };

        Utilities.getDataFromAPI(getApplicationContext(), Urls.weatherData, weatherDataCallback);
    }

    private void fillData(JSONObject data) {
        LinearLayout loader = (LinearLayout) findViewById(R.id.am_loader);
        LinearLayout infoContainer = (LinearLayout) findViewById(R.id.am_info_container);
        GlobalTextView weatherText = (GlobalTextView) infoContainer.findViewById(R.id.am_info_text);
        GlobalTextView tempHigh = (GlobalTextView) infoContainer.findViewById(R.id.am_info_temp_high);
        GlobalTextView tempLow = (GlobalTextView) infoContainer.findViewById(R.id.am_info_temp_low);
        GlobalTextView tempCurrent = (GlobalTextView) infoContainer.findViewById(R.id.am_info_temp_current);
        GlobalTextView dateView = (GlobalTextView) findViewById(R.id.am_date);
        GlobalTextView forecastText = (GlobalTextView) findViewById(R.id.am_weekly_forecast_text);
        LinearLayout forecastContainer = (LinearLayout) findViewById(R.id.am_forecast_layout);
        GlobalIconView iconView = (GlobalIconView) findViewById(R.id.am_icon);


        loader.setVisibility(View.GONE);
        forecastContainer.setVisibility(View.VISIBLE);
        infoContainer.setVisibility(View.VISIBLE);
        forecastText.setVisibility(View.VISIBLE);

        JSONObject item = data.optJSONObject("query")
                .optJSONObject("results")
                .optJSONObject("channel")
                .optJSONObject("item");
        JSONObject condition = item.optJSONObject("condition");
        JSONArray forecast = item.optJSONArray("forecast");

        tempHigh.setText(forecast.optJSONObject(0).optString("high"));
        tempLow.setText(forecast.optJSONObject(0).optString("low"));

        tempCurrent.setText(condition.optString("temp"));
        weatherText.setText(condition.optString("text"));

        DateFormat dateFormat = new SimpleDateFormat("dd MMM");
        Date date = new Date();

        dateView.setText("TODAY, " + dateFormat.format(date));

        if (Utilities.iconMap.optJSONObject(condition.optString("text")) != null) {
            iconView.setText(Html.fromHtml(Utilities.iconMap.optJSONObject(condition.optString("text")).optString("icon")));
        }

        fillForecast(forecast);
    }

    private void fillForecast(JSONArray forecast) {
        ViewPager forecastPager = (ViewPager) findViewById(R.id.am_view_pager);
        forecastPager.setAdapter(new ForecastPagerAdapter(getSupportFragmentManager(), forecast));

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(forecastPager);
    }


    private class ForecastPagerAdapter extends FragmentPagerAdapter {

        protected JSONArray data;

        public ForecastPagerAdapter(FragmentManager fm, JSONArray data) {
            super(fm);
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.length();
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new WeatherCardFragment();
            Bundle args = new Bundle();
            args.putString("text", data.optJSONObject(i).optString("text"));
            args.putString("high", data.optJSONObject(i).optString("high"));
            args.putString("low", data.optJSONObject(i).optString("low"));
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public CharSequence getPageTitle (int position) {
            return data.optJSONObject(position).optString("day");
        }
    }
}
