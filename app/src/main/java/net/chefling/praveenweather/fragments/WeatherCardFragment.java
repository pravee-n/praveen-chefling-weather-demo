package net.chefling.praveenweather.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.chefling.praveenweather.R;
import net.chefling.praveenweather.views.GlobalTextView;
import net.chefling.praveenweather.utils.Utilities;
import net.chefling.praveenweather.views.GlobalIconView;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeatherCardFragment extends Fragment {

    View view;

    public WeatherCardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_weather_card, container, false);
        String tempHigh = getArguments().getString("high");
        String tempLow = getArguments().getString("low");
        String text = getArguments().getString("text");
        GlobalTextView tempHighView = (GlobalTextView) view.findViewById(R.id.wfc_temp_high);
        GlobalTextView tempLowView = (GlobalTextView) view.findViewById(R.id.wfc_temp_low);
        GlobalTextView textView = (GlobalTextView) view.findViewById(R.id.wfc_text);
        GlobalIconView iconView = (GlobalIconView) view.findViewById(R.id.wfc_icon);

        tempHighView.setText(tempHigh);
        tempLowView.setText(tempLow);
        textView.setText(text);

        if (Utilities.iconMap.optJSONObject(text) != null) {
            iconView.setText(Html.fromHtml(Utilities.iconMap.optJSONObject(text).optString("icon")));
            iconView.setTextColor(Integer.parseInt(Utilities.iconMap.optJSONObject(text).optString("color")));
        }

        return view;
    }

}
