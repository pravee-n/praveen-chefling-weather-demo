package net.chefling.praveenweather.utils;

import org.json.JSONObject;

/**
 * Created by praveen on 27/10/16.
 */

public interface BasicDataCallback {
    public void callback(JSONObject data);
}
