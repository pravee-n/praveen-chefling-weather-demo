package net.chefling.praveenweather.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import net.chefling.praveenweather.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by praveen on 27/10/16.
 */

public class Utilities {

    private static Context context;
    public static Typeface fontTypeFaceRegular;
    public static Typeface fontTypeFaceLight;
    public static Typeface fontTypeFaceUltraLight;
    public static Typeface fontTypeFaceBold;
    public static Typeface iconFontTypeFace;

    public static JSONObject iconMap;

    public static void setContext(Context ctx) {
        context = ctx;
    }

    public static void setIconMap() {
        iconMap = new JSONObject();
        try {
            iconMap.put("Breezy", new JSONObject().put("icon", "&#xf4b6;").put("color", ContextCompat.getColor(context, R.color.sun_yellow)));
            iconMap.put("Scattered Showers", new JSONObject().put("icon", "&#xf494;").put("color", ContextCompat.getColor(context, R.color.blue)));
            iconMap.put("Showers", new JSONObject().put("icon", "&#xf494;").put("color", ContextCompat.getColor(context, R.color.blue)));
            iconMap.put("Cloudy", new JSONObject().put("icon", "&#xf409;").put("color", ContextCompat.getColor(context, R.color.blue)));
            iconMap.put("Partly Cloudy", new JSONObject().put("icon", "&#xf475;").put("color", ContextCompat.getColor(context, R.color.sun_yellow)));
            iconMap.put("Mostly Cloudy", new JSONObject().put("icon", "&#xf475;").put("color", ContextCompat.getColor(context, R.color.sun_yellow)));
            iconMap.put("Scattered Thunderstorms", new JSONObject().put("icon", "&#xf4bc;").put("color", ContextCompat.getColor(context, R.color.blue)));
            iconMap.put("Clear", new JSONObject().put("icon", "&#xf4b6;").put("color", ContextCompat.getColor(context, R.color.sun_yellow)));
            iconMap.put("Mostly Sunny", new JSONObject().put("icon", "&#xf4b6;").put("color", ContextCompat.getColor(context, R.color.sun_yellow)));
            iconMap.put("Sunny", new JSONObject().put("icon", "&#xf4b6;").put("color", ContextCompat.getColor(context, R.color.sun_yellow)));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static Context getContext() {
        return context;
    }

    public static void setFontTypeFace() {
        fontTypeFaceRegular = Typeface.createFromAsset( context.getAssets(), "Roboto-Regular.ttf" );
        fontTypeFaceBold = Typeface.createFromAsset( context.getAssets(), "Roboto-Medium.ttf" );
        fontTypeFaceLight = Typeface.createFromAsset( context.getAssets(), "Roboto-Light.ttf" );
        fontTypeFaceUltraLight = Typeface.createFromAsset( context.getAssets(), "Roboto-Thin.ttf" );
    }

    public static void setIconFontTypeFace() {
        iconFontTypeFace = Typeface.createFromAsset(  context.getAssets(), "ionicons.ttf" );
    }

    public static void getDataFromAPI(Context context, String URL, final BasicDataCallback successCallback) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if (successCallback != null) {
                            successCallback.callback(response);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                });

        VolleySingleton.getInstance(context).addToRequestQueue(jsObjRequest);
    }
}
