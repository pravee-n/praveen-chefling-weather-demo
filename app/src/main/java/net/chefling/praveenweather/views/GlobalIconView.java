package net.chefling.praveenweather.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import net.chefling.praveenweather.utils.Utilities;

/**
 * Created by praveen on 27/10/16.
 */

public class GlobalIconView extends TextView {
    public GlobalIconView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GlobalIconView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    void init() {
        this.setTypeface(Utilities.iconFontTypeFace);
    }
}
