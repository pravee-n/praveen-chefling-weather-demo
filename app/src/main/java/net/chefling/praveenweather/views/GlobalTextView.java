package net.chefling.praveenweather.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import net.chefling.praveenweather.R;
import net.chefling.praveenweather.utils.Utilities;

/**
 * Created by praveen on 27/10/16.
 */

public class GlobalTextView extends TextView {


    public GlobalTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public GlobalTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    void init(AttributeSet attrs) {
        String fontWeight;
        if (attrs != null) {
            TypedArray a = Utilities.getContext().getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.GlobalTextView,
                    0, 0);
            try {
                fontWeight = a.getString(R.styleable.GlobalTextView_fontWeight);
            } finally {
                a.recycle();
            }

            if (fontWeight == null || fontWeight.equals(getResources().getString(R.string.font_weight_regular))) {
                this.setTypeface(Utilities.fontTypeFaceRegular);
            }
            else if (fontWeight == null || fontWeight.equals(getResources().getString(R.string.font_weight_ultra_light))) {
                this.setTypeface(Utilities.fontTypeFaceUltraLight);
            }
            else if (fontWeight.equals(getResources().getString(R.string.font_weight_semi_bold))) {
                this.setTypeface(Utilities.fontTypeFaceBold);
            }
            else if (fontWeight.equals(getResources().getString(R.string.font_weight_light))) {
                this.setTypeface(Utilities.fontTypeFaceLight);
            }
        }
        else {
            this.setTypeface(Utilities.fontTypeFaceRegular);
        }
    }
}
